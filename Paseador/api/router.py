from rest_framework.routers import DefaultRouter
from Paseador.api.views import DogWalkerApiViewSet

routers_dogWalker = DefaultRouter()

routers_dogWalker.register(prefix='dogWalker', basename='dogWalker', viewset=DogWalkerApiViewSet)
