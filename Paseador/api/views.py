from rest_framework.viewsets import ModelViewSet
from Paseador.models import DogWalker
from Paseador.api.serializers import DogWalkerSerializer


class DogWalkerApiViewSet(ModelViewSet):
    serializer_class = DogWalkerSerializer
    queryset = DogWalker.objects.all()
