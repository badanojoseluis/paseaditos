from rest_framework.serializers import ModelSerializer
from Paseador.models import DogWalker


class DogWalkerSerializer(ModelSerializer):
    class Meta:
        model = DogWalker
        fields = '__all__'
