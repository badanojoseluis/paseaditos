from django.db import models


class DogWalker(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100, blank=True)
    phone = models.CharField(max_length=100, blank=True)
    mail = models.CharField(max_length=100, blank=True)


    def __str__(self) -> str:
            return f'{self.first_name} {self.last_name}'

    class Meta:
        verbose_name = "Paseador"
        verbose_name_plural = "Paseadores"