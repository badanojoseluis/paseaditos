# Paseaditos

El projeto consiste en un sistema para paseadores de perros. La idea principal es poder registrar :
Paseadores,
Mascotas,
Paseos por mascota,
Pagos.

De esta forma podran tomar asistencia y llevar un control de cuanto cobrar mensual (muchos paseadores cobran dependiendo las veces que el perro sale)

El reporte extra que dejamos en un endpoint (marcado abajo) saca un reporte de la cantidad de paseos por una mascota dada.


## Migrations

Remember run the migrations before push any changes.

```
python manage.py makemigrations
python manage.py migrate

```

## Start project

To start the project run the command
```
/> python manage.py runserver ```
```

## Admin

To open the admin user go to `localhost:8000/admin`

```
User: admin
Password: admin
```


## Reporter

Metodo que trae un reporte custom ( Cantidad de paseos por mascota): http://127.0.0.1:8000/dogWalk/report/
```
curl -X GET "http://127.0.0.1:8000/dogWalk/report/" -H "accept: application/json" -H "X-CSRFToken: ch1bISlQlwt54nBfeBpSfGbYDEvZ7p2L0IQS5omKaIonGzfOyOZDYY22eJBPCzTk"
```
