from django.db import models

from Perros.models import Dogs


class Payment(models.Model):
    dog = models.ForeignKey(
        Dogs,
        related_name='payments',
        on_delete=models.SET_NULL,
        null=True
    )
    datetime = models.DateTimeField()
    amount = models.DecimalField(max_digits=100, decimal_places=2, blank=True)
    payment_method = models.CharField(max_length=100, blank=True)

    def __str__(self) -> str:
        return f'Monto {self.amount}$ de la mascota {self.dog.first_name}'

    class Meta:
        verbose_name = "Pago"
        verbose_name_plural = "Pagos"
