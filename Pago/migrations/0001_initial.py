# Generated by Django 4.0.4 on 2022-05-15 18:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('Perros', '0004_rename_race_dogs_breed'),
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('datetime', models.DateTimeField()),
                ('amount', models.FloatField(blank=True)),
                ('payment_method', models.CharField(blank=True, max_length=100)),
                ('dog', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='payments', to='Perros.dogs')),
            ],
            options={
                'verbose_name': 'Pago',
                'verbose_name_plural': 'Pagos',
            },
        ),
    ]
