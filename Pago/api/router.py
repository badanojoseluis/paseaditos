from rest_framework.routers import DefaultRouter

from Pago.api.views import PaymentApiViewSet

routers_payment = DefaultRouter()

routers_payment.register(prefix='payment', basename='payment', viewset=PaymentApiViewSet)
