from rest_framework.viewsets import ModelViewSet

from Pago.api.serializers import PaymentSerializer
from Pago.models import Payment


class PaymentApiViewSet(ModelViewSet):
    serializer_class = PaymentSerializer
    queryset = Payment.objects.all()
