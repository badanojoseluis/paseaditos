from django.db import models
# from django.utils.translation import gettext_lazy as _

from Perros.models import Dogs
from Paseador.models import DogWalker


class DogWalk(models.Model):
    pickup_time = models.TimeField()
    return_time = models.TimeField()

    dog = models.ForeignKey(
        Dogs,
        related_name='dogs',
        on_delete=models.SET_NULL,
        null=True
    )

    DogWalker = models.ForeignKey(
        DogWalker,
        related_name='DogWalker',
        on_delete=models.SET_NULL,
        null=True
    )

    def __str__(self) -> str:
        return f'{self.dog} -  recoger: {self.pickup_time}  - devolver: {self.return_time}'

    class Meta:
        verbose_name = "PaseoMascota"
        verbose_name_plural = "PaseoMascotas"
