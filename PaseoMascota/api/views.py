from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from PaseoMascota.models import DogWalk
from PaseoMascota.api.serializers import DogWalkSerializer
from django.http import HttpResponse
from django.db.models import Count
import json
from django.core.serializers.json import DjangoJSONEncoder


class DogWalkApiViewSet(ModelViewSet):
    serializer_class = DogWalkSerializer
    queryset = DogWalk.objects.all()

    @action(detail=False)
    def report(self, request, **kwargs):
        SomeModel_json = DogWalk.objects.values('dog').annotate(dcount=Count('dog')).order_by()
        structure = json.dumps(list(SomeModel_json), cls=DjangoJSONEncoder)
        return HttpResponse(structure)
