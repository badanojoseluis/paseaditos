from rest_framework.serializers import ModelSerializer

from PaseoMascota.models import DogWalk


class DogWalkSerializer(ModelSerializer):
    class Meta:
        model = DogWalk
        fields = '__all__'
