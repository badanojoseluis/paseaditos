from rest_framework.routers import DefaultRouter

from PaseoMascota.api.views import DogWalkApiViewSet

routers_dogWalk = DefaultRouter()

routers_dogWalk.register(prefix='dogWalk', basename='dogWalk', viewset=DogWalkApiViewSet)
