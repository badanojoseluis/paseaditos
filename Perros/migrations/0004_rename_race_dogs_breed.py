# Generated by Django 4.0.4 on 2022-05-02 23:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Perros', '0003_alter_dogs_owner_alter_dogs_owner_mail_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='dogs',
            old_name='race',
            new_name='breed',
        ),
    ]
