from rest_framework.routers import DefaultRouter
from Perros.api.views import DogsApiViewSet

routers_dogs = DefaultRouter()

routers_dogs.register(prefix='dogs', basename='dogs', viewset=DogsApiViewSet)
