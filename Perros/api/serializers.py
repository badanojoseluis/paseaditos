from rest_framework.serializers import ModelSerializer
from Perros.models import Dogs


class DogsSerializer(ModelSerializer):
    class Meta:
        model = Dogs
        fields = '__all__'
