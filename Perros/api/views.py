from rest_framework.viewsets import ModelViewSet
from Perros.models import Dogs
from Perros.api.serializers import DogsSerializer


class DogsApiViewSet(ModelViewSet):
    serializer_class = DogsSerializer
    queryset = Dogs.objects.all()
