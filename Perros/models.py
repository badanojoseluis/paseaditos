from datetime import date

from django.db import models

class Dogs(models.Model):
    # Id_Mascota = models.CharField(max_length=10, unique=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100, blank=True)
    breed = models.CharField(max_length=30, blank=True) # TODO: Crear app para razas?
    sex = models.CharField(max_length=10, default='M')
    aggressive = models.BooleanField(default=False)
    vaccinated = models.BooleanField(default=False)
    birthday = models.DateField()
    address = models.CharField(max_length=100)  # Tendriamos que hacer un objeto para esto?
    owner = models.CharField(max_length=100, blank=True)
    owner_phone = models.CharField(max_length=100, blank=True)
    owner_mail = models.CharField(max_length=100, blank=True)

    # Id_Mascota
    # Id_Paseador
    # Nombre
    # Sexo
    # Agresivo(Si / No)
    # Vacunas(Si / No)
    # Edad -> este campo lo deberiamos calcular
    # Direccion
    # Nombre_Tutor
    # Telefono_Tutor
    # Email_Tutor

    # event = models.ForeignKey(
    #     Event,
    #     related_name='attendees',
    #     on_delete=models.DO_NOTHING,
    #     null=True
    # )

    def __str__(self) -> str:
        return f'{self.first_name} {self.last_name} age: {self.age()} owner: {self.owner}'

    def age(self):
        today = date.today()
        born = self.birthday
        rest = 1 if (today.month, today.day) < (born.month, born.day) else 0
        return today.year - born.year - rest

    class Meta:
        verbose_name = "Perro"
        verbose_name_plural = "Perros"
